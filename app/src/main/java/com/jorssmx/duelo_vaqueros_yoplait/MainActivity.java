package com.jorssmx.duelo_vaqueros_yoplait;

import android.app.AppComponentFactory;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorEventListener2;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.JobIntentService;

import com.jorssmx.duelo_vaqueros_yoplait.R;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private ImageView gunView;
    private SensorManager sensorManager;
    /**
     * Referencia al podometro
     */
    private Sensor stepDetectorSensor;
    private byte step;

    private ExecutorService singleThreadProducer;

    private DrawTimer asyCounter;

    /**
     * Pasos a contar
     */
    public static final byte SECONDS_TO_COUNT = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gunView = findViewById(R.id.gun_iv);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null) {
            stepDetectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        }
        if (stepDetectorSensor == null) {
            sensorManager = null;
        }
    }

    /**
     * Metodo para que use la pantalla completa la aplicacion.
     *
     * @param hasFocus Whether the window of this activity has focus.
     *                 agregado por jorge.
     */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_LANDSCAPE);//Linea para mantener en Horizontal
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY|
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE|
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION|
                    View.SYSTEM_UI_FLAG_FULLSCREEN|
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION|
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            );
        }
    }

    /**
     * Metodo en cual se registra la escucha del sensor cuando el usuario preciona el inico del duelo.
     * agregado por jorge.
     */
    private void checkStepSensor() {
        if (sensorManager == null) {
            startTimer();
            return;
        }
        // checar esta linea que es del minuto 32:35 del video.
        sensorManager.registerListener(SensorEventListener, this, stepDetectorSensor,
                                                           SensorManager.SENSOR_DELAY_GAME);
    }

    private void startTimer(){
        if(singleThreadProducer== null){
            singleThreadProducer= Executors.newSingleThreadExecutor();
        }
        asyCounter= new DrawTimer(gunView, SECONDS_TO_COUNT);
        singleThreadProducer.execute(asyCounter);

    }

    /**
     * Metodo para utilizar cuando se tiene una lectura que ofrecer.
     *
     * @param event - el sensor a utilizar.
     *              agregado por jorge.
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        step++;
        if (step >= 3) {
            sensorManager.unregisterListener(this);
            gunView.setVisibility(View.VISIBLE);
            step = 0;
        }
    }

    /**
     * Metodo que indica que la presicion del sensor a cambiado y podemos esperar
     * variaciones en la lectura.
     *
     * @param sensor   - el sensor a utilizar.
     * @param accuracy - para el grado de presicion que adquiere el sensor.
     *                 agregado por jorge - en el video lo dejo asi tal cual 'vacio'.
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    public void finalCountdown(View startButton) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); //Mantiene la pantalla encendida
        startButton.setVisibility(View.INVISIBLE);
        checkStepSensor();
    }


    /**
     * Método para  "disparar" el arma.
     *
     * @param gun - arma a utilizar
     */
    public void fire(View gun){
        JobIntentService.enqueueWork(this, SoundPlayer.class, 0,
                new Intent(SoundPlayer.ACTION_FIRE));
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run(){
                init();
            }

        }, 3000);
    }
    private void KilllCounter(){
        if (sensorManager != null) {
            sensorManager.unregisterListener(this);
        } else if (singleThreadProducer != null) {
            singleThreadProducer.shutdownNow();
            singleThreadProducer = null;

        }

    }

    @Override
    protected void onPause() {
        KilllCounter();
        super.onPause();

    }















}
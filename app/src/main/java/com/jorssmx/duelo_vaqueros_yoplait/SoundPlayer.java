package com.jorssmx.duelo_vaqueros_yoplait;

import android.app.ForegroundServiceTypeException;
import android.media.MediaPlayer;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

public class SoundPlayer extends JobIntentService {

    public static final String ACTION_FIRE ="com.jorssmx.duelo_vaqueros_yoplait.FIRE";
    private static final byte JOB_ID=0;

    @Override
    protected void onHandleWork(@NonNull Intent intent){
        String action = intent.getAction();
        switch (action){
            case ACTION_FIRE:
                MediaPlayer soundPlayer = MediaPlayer.create(this, R.raw.fire);
                SoundPlayer.setOnCompletionListener(
                        new MediaPlayer.OnCompletionListener(){
                            @Override
                            public void onCompletion(MediaPlayer mp){
                                mp.release();
                            }
                        }
                );
                soundPlayer.start();
                break;
            default:
                log.d(SoundPlayer.class.getSimpleName(), "unrecognized action" + action);
        }
    }


}
